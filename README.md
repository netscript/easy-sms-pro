基于 easy-sms 扩展的包
=================== 


 ![license](https://img.shields.io/badge/license-MIT-yellow.svg)
 ![license](https://img.shields.io/badge/php-^7.1-green.svg)
 ![license](https://img.shields.io/badge/composer-lib-blue.svg)
 ![license](https://img.shields.io/badge/function-sms-red.svg)
 ![license](https://img.shields.io/badge/testing-phpunit-green.svg)


> 扩展支持 大汉三通通信平台
 
 [平台文档地址](http://help.dahantc.com/docs/oss/1apkfg3aqjoot.html)

> 安装 (集成到现有easy-sms）

    composer require weblinuxgame/sms

  #### 注意: php >=7.1
 
> 使用demo  

eg:  
    
    $config = [];//相关配置
    $app = new EasySms($config); 
    $phone='';// 手机号
    $data = [
         'content' => '测试验证码' . rand(1000, 9999),// 发生内容
         'template' => '122xx21323xx',// 模板id
         'data' => [
              'sendtime' => time(),// 定时发送(天)[选填]
              'subcode' => '7889', // 子签名[按平台配置选填]
         ],
    ];
    $app->send($phone,$data, ['dahansantong','baidu','aliyun']);
    

eg2:  
 
     $config = [];//相关配置
     $app = new EasySms($config); 
     $phone='';// 手机号
     $data = [
          'content' => '测试验证码' . rand(1000, 9999),// 发生内容
          'template' => '122xx21323xx',// 模板id
          'data' => [
               'sendtime' => time(),// 定时发送(天)[选填]
               'subcode' => '7889', // 子签名[按平台配置选填]
          ],
     ];
     // 自定义扩展网关
     $app->extend('my-sms',function($config){
        return new \WebLinuxGame\Sms\Gateways\DaHanSanTongGateway($config);
     });
     $app->send($phone,$data, ['my-sms']);   
    
> 配置demo  
    
   config.php [可以沿用easy-sms]  
   如果已有相关配置,只需追加 gateways对应   
   dahansantong 的内容 和 default中的gateways
   
    return [
        // HTTP 请求的超时时间（秒）
        'timeout' => 5.0,
    
        // 默认发送配置
        'default' => [
            // 网关调用策略，默认：顺序调用
            'strategy' => \Overtrue\EasySms\Strategies\OrderStrategy::class,
    
            // 默认可用的发送网关
            'gateways' => [
                'dahansantong', 'yunpian', 'aliyun',
            ],
        ],
        // 可用的网关配置
        'gateways' => [
            'errorlog' => [
                'file' => '/tmp/easy-sms.log',
            ],
            'dahansantong' => [
                'account' => env('account', "dh1234"),
                'password' => md5(env('password', "%eNTE67G")),
                'subcode' => env('subcode', "853101"),
            ],
            'yunpian' => [
                'api_key' => '824f0ff2f71cab52936axxxxxxxxxx',
            ],
            'aliyun' => [
                'access_key_id' => '',
                'access_key_secret' => '',
                'sign_name' => '',
            ],
        ],
    ];    
  
> 感谢 easy-sms 项目

####  [easy-sms github](https://github.com/overtrue/easy-sms)
 
