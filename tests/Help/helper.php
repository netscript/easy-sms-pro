<?php
/**
 * Project Sms
 * file: helper.php
 * User: weblinuxgame
 * Date: 2019/6/16
 * Time: 16:41
 */


if (!function_exists('env')) {

    /**
     * 环境
     * @param string $key
     * @param null $default
     * @return array|false|null|string
     */
    function env(string $key, $default = null)
    {
        return getenv($key) ?? $default;
    }

}

if (!function_exists('dump')) {

    /**
     * 调试输出
     * @param mixed ...$args
     * @return void
     */
    function dump(...$args)
    {
        $count = func_get_args();
        if ($count == 0) {
            return;
        }
        if (1 == $count) {
            var_dump(current($args));
        }
        if ($count > 1) {
            foreach ($args as $var) {
                var_dump($var);
            }
        }
        return;
    }

}


if (!function_exists('dd')) {

    /**
     * 调试输出
     * @param mixed ...$args
     */
    function dd(...$args)
    {
        $count = func_get_args();
        if ($count == 0) {
            return exit(0);
        }
        if (1 == $count) {
            var_dump(current($args));
        }
        if ($count > 1) {
            foreach ($args as $var) {
                var_dump($var);
            }
        }
        return exit(0);
    }

}

