<?php
/**
 * Project Sms
 * file: TestDaHanSanTongSms.php
 * User: weblinuxgame
 * Date: 2019/6/16
 * Time: 16:14
 */

namespace WebLinuxGame\Sms\Tests\Unit;

use Overtrue\EasySms\EasySms;
use Overtrue\EasySms\Exceptions\GatewayErrorException;
use Overtrue\EasySms\Gateways\Gateway;
use WbeLinuxGame\Sms\Tests\TestCase;
use Overtrue\EasySms\Gateways\DahansantongGateway;

/**
 * Class TestDaHanSanTongSms
 * @package WebLinuxGame\Sms\Tests\Unit
 */
class TestDaHanSanTongSms extends TestCase
{
    public function testApp()
    {
        $this->app()->extend('my-sms', function ($config) {
            return new \WebLinuxGame\Sms\Gateways\DaHanSanTongGateway($config);
        });
        $this->assertTrue($this->app() instanceof EasySms, '应用对象获取失败');
    }

    public function testDaHanSanTongGateway()
    {
        $gateway = $this->app()->gateway('dahansantong');
        $this->assertTrue($gateway instanceof DahansantongGateway, '网关获取失败');
    }

    public function testGatewaySend()
    {
        // $msg = '嘿，小何，你有1条新的面试邀请！面试职位：golang工程师。请登录小程序 “猎多多” 去看看吧';
        // $msg = '【猎多多】嘿，小何，面试官确认与你约面，很快会向你发起面试安排！请登录小程序 “猎多多” 去看看吧';
        $gateway = $this->app()->gateway('dahansantong');
        if ($gateway instanceof DahansantongGateway) {
            $config = $gateway->getConfig();
            $params = [
                'sign' => "【猎多多】",// 签名
                'account' => $config->get('account'),// 账号
                'password' => $config->get('password'),// 密码
                'phones' => env('phone'), // 手机号集合
                'content' => $msg,// 内容
                // 定时发送时间，格式yyyyMMddHHmm，为空或早于当前时间则立即发送
                'sendtime' => '',
                'subcode' => '', // 子签名
            ];
            $result = $gateway->service()->sms->send($params);
            dump($result);
            $this->assertTrue($gateway->service()->sms::isSuccess($result), $result['desc']);
            $this->assertTrue(is_array($result));
        }
        dump(1233);
        $this->assertEmpty($gateway);
    }

    public function testSend()
    {
        $result = [];
        // 6e6f7e28e8374b989c10817d315570b3
        $msg = '嘿，小何，你有1条新的面试邀请！面试职位：golang工程师。请登录小程序 “猎多多” 去看看吧';
        $params = [
            'data' => [
                'sign' => "【猎多多】",
            ],
            'phones' => env('phone'), // 手机号集合
            'content' => $msg,// 内容
        ];
        $phone = env('phone');
        try {
            $result = $this->app()->send($phone, $params, ['dahansantong']);
            dump($result);
        } catch (\Exception $e) {
            dump($e->getMessage());
            $arr = $e->getExceptions();
            foreach ($arr as $key => $result) {
                dump($key);
                dump($result instanceof GatewayErrorException, $result->raw);
            }
        }
        $this->assertTrue(!empty($result));
    }

    public function testCheck()
    {
        $phones = [];
        $gateway = $this->app()->gateway('dahansantong');
        if ($gateway instanceof DahansantongGateway) {
            $result = $gateway->service()->sms->checkBlackList($phones);
            dump($result);
            $this->assertTrue($gateway::isSuccess($result), $result['desc']);
            $this->assertTrue(is_array($result));
        }
        dump(1233);
        $this->assertEmpty($gateway);
    }

    public function testGetReports()
    {
        $gateway = $this->app()->gateway('dahansantong');
        if ($gateway instanceof DahansantongGateway) {
            $reports = $gateway->service()->sms->getSmsReport();
            dump($reports);
        }
        $this->assertNotEmpty($reports ?? null, '查询失败');
    }

    public function testGetTemplateShow()
    {
        $templateIds = ['2c90819667c5e6d4016b6a056bcf2c58'];
        $app = $this->app()->gateway('dahansantong');
        if ($app instanceof DahansantongGateway) {
            $result = $app->service()->template->show($templateIds);
            dump($result);
            $this->assertTrue(is_array($result));
        }
        $this->assertTrue($app instanceof Gateway);
    }

    public function testTemplateCreate()
    {
        // 2c90819667c5e6d4016b6a056bcf2c58
        $app = $this->app()->gateway('dahansantong');
        if ($app instanceof DahansantongGateway) {
            $result = $app->service()->template->create('欢迎使用${1,30}猎多多小程序,下载app可以获取更多惊喜哦!',null,'引导下载');
            dump($result);
            $this->assertTrue(is_array($result));
        }
        $this->assertTrue($app instanceof Gateway);
    }

    public function testDeleteTemplate()
    {
        $templateIds = ['2c90819667c5e6d4016b6a056bcf2c58'];
        $app = $this->app()->gateway('dahansantong');
        if ($app instanceof DahansantongGateway) {
            $result = $app->service()->template->delete($templateIds);
            dump($result);
            $this->assertTrue(is_array($result));
        }
        $this->assertTrue($app instanceof Gateway);
    }
}