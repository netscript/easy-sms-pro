<?php
/**
 * Project Sms
 * file: config.php
 * User: weblinuxgame
 * Date: 2019/6/16
 * Time: 16:27
 */

return [
    // HTTP 请求的超时时间（秒）
    'timeout' => 5.0,

    // 默认发送配置
    'default' => [
        // 网关调用策略，默认：顺序调用
        'strategy' => \Overtrue\EasySms\Strategies\OrderStrategy::class,

        // 默认可用的发送网关
        'gateways' => [
            'dahansantong', 'yunpian', 'aliyun',
        ],
    ],
    // 可用的网关配置
    'gateways' => [
        'errorlog' => [
            'file' => '/tmp/easy-sms.log',
        ],
        'dahansantong' => [
            'account' => env('account', "dh1234"),
            'password' => md5(env('password', "%eNTE67G")),
            'subcode' => env('subcode', "853101"),
            'sign' => env('sign', "【大汉三通】"),
        ],
        'yunpian' => [
            'api_key' => '824f0ff2f71cab52936axxxxxxxxxx',
        ],
        'aliyun' => [
            'access_key_id' => '',
            'access_key_secret' => '',
            'sign_name' => '',
        ],
    ],
];