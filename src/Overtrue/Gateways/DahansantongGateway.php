<?php

namespace Overtrue\EasySms\Gateways;

use WebLinuxGame\Sms\Gateways\DaHanSanTongGateway as Gateway;

/**
 * 大汉三通
 * Class DaHanSanTongSmsGetWay.
 *
 * @see http://help.dahantc.com/docs/oss/1apkfg3aqjoot.html
 */
class DahansantongGateway extends Gateway{}