<?php

namespace WebLinuxGame\Sms\Gateways;

use Overtrue\EasySms\Support\Config;
use Overtrue\EasySms\Gateways\Gateway;
use Overtrue\EasySms\Contracts\MessageInterface;
use WebLinuxGame\Sms\Clients\DaHanSanTong\Service;
use Overtrue\EasySms\Contracts\PhoneNumberInterface;
use Overtrue\EasySms\Exceptions\GatewayErrorException;

/**
 * 大汉三通
 * Class DaHanSanTongSmsGetWay.
 * @see http://help.dahantc.com/docs/oss/1apkfg3aqjoot.html
 */
class DaHanSanTongGateway extends Gateway
{
    /**
     * @var Service|null $oService
     */
    protected $oService = null;

    /**
     * 构建
     * DaHanSanTongGateway constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        parent::__construct($config);
        $this->service();
    }

    /**
     * 短信发送
     * @param \Overtrue\EasySms\Contracts\PhoneNumberInterface $to
     * @param \Overtrue\EasySms\Contracts\MessageInterface $message
     * @param \Overtrue\EasySms\Support\Config $config
     *
     * @return array
     *
     * @throws \Overtrue\EasySms\Exceptions\GatewayErrorException ;
     */
    public function send(PhoneNumberInterface $to, MessageInterface $message, Config $config)
    {
        $client = $this->service()->sms;
        $result = $client->send($client->params($to, $message, $config));
        if (!$client::isSuccess($result)) {
            throw new GatewayErrorException($result['desc'], 400, $result);
        }
        return $result;
    }

    /**
     * 获取客户端
     * @return null|Service
     */
    public function service(): ?Service
    {
        if (empty($this->oService)) {
            $this->oService = new Service($this);
        }
        return $this->oService;
    }

    /**
     * 代理其他功能接口
     * @param $name
     * @param $arguments
     * @return null
     */
    public function __call($name, $arguments)
    {
        $client = $this->service();
        if (!empty($client) && method_exists($client, $name)) {
            if (empty($arguments)) {

                return $client->{$name}();
            }
            return $client->{$name}(...$arguments);
        }
        return null;
    }

    /**
     * 获取
     * @param $name
     * @return null
     */
    public function __get($name)
    {
        return $this->oService->{$name};
    }

    /**
     * 是否存在
     * @param $name
     * @return bool
     */
    public function __isset($name)
    {
        return $this->oService->__isset($name);
    }


}