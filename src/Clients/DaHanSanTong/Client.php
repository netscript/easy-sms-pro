<?php
/**
 * Project Sms
 * file: BaseClient.php
 * User: weblinuxgame
 * Date: 2019/6/18
 * Time: 16:02
 */

namespace WebLinuxGame\Sms\Clients\DaHanSanTong;


use Overtrue\EasySms\Gateways\Gateway;
use Overtrue\EasySms\Support\Config;
use Overtrue\EasySms\Traits\HasHttpRequest;

/**
 * 基础客户端
 * Class Client
 * @package WebLinuxGame\Sms\Clients\DaHanSanTong
 */
abstract class Client
{
    use HasHttpRequest;
    const SUCCESS_CODE = 0;
    const SIGN = "【大汉三通】";
    const SEND_TIME_FORMAT = 'YmdHi';// 定时(天)发送
    const ENDPOINT_TEMPLATE = "http://wt.3tong.net/json/sms";

    protected $config;
    protected $gateway;

    /**
     * 客户端
     * Client constructor.
     * @param Config $config
     * @param Gateway|null $gateway
     */
    public function __construct(Config $config, Gateway $gateway = null)
    {
        $this->config = $config;
        if (!is_null($gateway)) {
            $this->gateway = $gateway;
        }
    }

    /**
     * 数组转换
     * @param $result
     * @return array
     */
    protected static function toArray($result): array
    {
        if (is_array($result)) {
            return $result;
        }
        if (is_object($result) && method_exists($result, 'toArray')) {
            return $result->toArray();
        }
        if (is_string($result) && !empty($result)) {
            if (preg_match('/^\{.+\}$/', $result)) {
                return (array)json_decode($result, true);
            }
        }
        return [];
    }

    /**
     * 是否有失败
     * @param array $result
     * @return bool
     */
    public static function hasFailPhones(array $result): bool
    {
        if (!empty($result['failPhones'])) {
            return true;
        }
        return false;
    }

    /**
     * 获取失败电话号
     * @param array $result
     * @return array
     */
    public static function getFailPhones(array $result): array
    {
        if (!self::hasFailPhones($result)) {
            return [];
        }
        return explode(',', $result['failPhones']);
    }

    /**
     * 获取请求
     * Build function api url.
     * @param string $path
     * @return string
     */
    protected function getApi(string $path)
    {
        if (empty($path)) {
            return self::ENDPOINT_TEMPLATE;
        }
        if ('/' == $path[0]) {
            return self::ENDPOINT_TEMPLATE . $path;
        }
        return self::ENDPOINT_TEMPLATE . '/' . $path;
    }

    /**
     * 获取签名
     * @param array $data
     * @return string
     */
    public function getSign(array $data): string
    {
        if (!empty($data['sign']) && is_string($data['sign']) && preg_match('/^【.+】$/', $data['sign'])) {
            return $data['sign'];
        }
        return $this->config->get('sign', '');
    }

    /**
     * 账号信息
     * @param Config|null $config
     * @return array
     */
    public function account(Config $config = null) :array
    {
        if(empty($config)){
            $config = $this->config;
        }
        return [
            'account' => $config->get('account'),
            'password' => $config->get('password'),
        ];
    }

    /**
     * 是否提交成功
     * @param array $ret
     * eg: [
     *  "msgid"=>"f02adaaa99c54ea58d626aac2f4ddfa8",
     *  "result"=>"0",
     *  "desc"=>"提交成功",
     *  "failPhones"=>"12935353535,110,130123123"
     * ]
     * @return bool
     */
    public static function isSuccess(array $ret): bool
    {
        if (empty($ret)) {
            return false;
        }
        if (self::SUCCESS_CODE == (int)($ret['result'] ?? -1)) {
            return true;
        }
        return false;
    }

}