<?php
/**
 * Project Sms
 * file: Servicee.php
 * User: weblinuxgame
 * Date: 2019/6/18
 * Time: 15:59
 */

namespace WebLinuxGame\Sms\Clients\DaHanSanTong\Template;

use WebLinuxGame\Sms\Clients\DaHanSanTong\ApiEnum;
use WebLinuxGame\Sms\Clients\DaHanSanTong\Client as BaseClient;

/**
 * 模板客户端
 * Class Client
 * @package WebLinuxGame\Sms\Clients\DaHanSanTong\Template
 */
class Client extends BaseClient
{
    /**
     * 查询模板
     * @param array $templateIds [模板id数组]
     * @return array
     */
    public function show(array $templateIds)
    {
        $data = array_merge($this->account(), [
            'templateIds' => implode(',', $templateIds),
        ]);
        return self::toArray($this->postJson(ApiEnum::API_TEMPLATE_QUERY, $data));
    }

    /**
     * 创建模板
     * @param string $content
     * @param string|null $sign
     * @param string $remark
     * eg:
     *  "content":"你好，这是白模板${1,10}示例",
     *  "sign":"【XXXX】",
     *  "remark":""
     * @return array
     */
    public function create(string $content, string $sign = null, string $remark = '')
    {
        $data = array_merge($this->account(), [
            'content' => $content,
            'sign' => $this->getSign(['sign' => $sign]),
            'remark' => $remark,
        ]);
        return self::toArray($this->postJson(ApiEnum::API_TEMPLATE_UPDATE, $data));
    }

    /**
     * 删除模板
     * @param array $templateIds
     * @return array
     */
    public function delete(array $templateIds)
    {
        $data = array_merge($this->account(), [
            'templateIds' => implode(',', $templateIds),
        ]);
        return self::toArray($this->postJson(ApiEnum::API_TEMPLATE_DELETE, $data));
    }

}