<?php
/**
 * Project Sms
 * file: Service.phpp
 * User: weblinuxgame
 * Date: 2019/6/18
 * Time: 14:35
 */

namespace WebLinuxGame\Sms\Clients\DaHanSanTong;


use Overtrue\EasySms\Gateways\Gateway;
use WebLinuxGame\Sms\Clients\DaHanSanTong\Sms\Client as SmsClient;
use WebLinuxGame\Sms\Clients\DaHanSanTong\Template\Client as TemplateClient;

/**
 * 客户端
 * Class Service
 * @property SmsClient $sms
 * @property TemplateClient $template
 * @package WebLinuxGame\Sms\Clients\DaHanSanTong
 */
class Service
{
    protected $sms;

    protected $template;

    protected $gateway;

    /**
     * 查询
     * Service constructor.
     * @param Gateway $gateway
     */
    public function __construct(Gateway $gateway)
    {
        $this->gateway = $gateway;
        $this->sms = new SmsClient($gateway->getConfig(), $gateway);
        $this->template = new TemplateClient($gateway->getConfig(), $gateway);
    }

    /**
     * 获取
     * @param $name
     * @return null
     */
    public function __get($name)
    {
        if (isset($this->{$name})) {
            return $this->{$name};
        }
        return null;
    }

    /**
     * 是否设置
     * @param $name
     * @return bool
     */
    public function __isset($name)
    {
        return !is_null($this->{$name} ?? null);
    }


}