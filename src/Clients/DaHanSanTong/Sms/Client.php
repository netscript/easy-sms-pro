<?php
/**
 * Project Sms
 * file: Client.php
 * User: weblinuxgame
 * Date: 2019/6/18
 * Time: 16:01
 */

namespace WebLinuxGame\Sms\Clients\DaHanSanTong\Sms;

use Overtrue\EasySms\Support\Config;
use Overtrue\EasySms\Contracts\MessageInterface;
use Overtrue\EasySms\Contracts\PhoneNumberInterface;
use WebLinuxGame\Sms\Clients\DaHanSanTong\Client as BaseClient;

/**
 * 短信客户端
 * Class Client
 * @package WebLinuxGame\Sms\Clients\DaHanSanTong\Sms
 */
class Client extends BaseClient
{
    /**
     * 发送短信
     * @param array $params
     * eg:[
     *  'sign' => '【大汉三通】',// 运行商签名
     *  'account' =>'account',// 账号
     *  'password' =>'password',// 密码的md5值
     *  'msgid' => '121212',// 模板id
     *  'phones' => '131xxxxxx,159xxxxx', // 手机号集合
     *  'content' => 'xxxx',// 内容
     *  'sendtime' => '19190101010101010',// [选填] 定时发送时间，格式yyyyMMddHHmm，为空或早于当前时间则立即发送
     *  'subcode' => '8679', // [选填]子签名
     * ]
     *
     * @return array
     * eg:
     *  [
     *    'result' => int,// 0 成功
     *    'desc' => string,// 中文提示: 成功
     *    'reports' => [ // 批量发送结果
     *       [
     *        'msgid' => '6e6f7e28e8374b989c10817d315570b3',// 消息id 小64位
     *        'phone' => '15100000000,
     *        'desc' => '发送成功',
     *        'wgcode'=>'0,
     *        'time' => '2019-06-18 17:33:29',
     *        'smsCount' => 1,
     *        'smsIndex'=>1,
     *       ],
     *      ....
     *   ]
     * ]
     *
     */
    public function send(array $params)
    {
        if (empty($params)) {
            return [];
        }
        return self::toArray($this->postJson($this->getApi('/Submit'), $params));
    }

    /**
     * 构建发送参数
     * @param PhoneNumberInterface $to
     * @param MessageInterface $message
     * @param Config $config
     * @return array
     */
    public function params(PhoneNumberInterface $to, MessageInterface $message, Config $config = null): array
    {
        if (empty($config)) {
            $config = $this->config;
        }
        $all = (array)$message->getData($this->gateway);
        $data = [
            'sign' => $this->getSign($all),// 签名
            'phones' => $to->getNumber(), // 手机号集合
            'content' => $this->content($message->getContent($this->gateway)),// 内容
        ];
        // 定时发送时间，格式yyyyMMddHHmm，为空或早于当前时间则立即发送
        $sendTime = $this->getSendTime($all);
        $subCode = $config->get('subcode'); // 子签名
        $msgId = $this->getMsgId($all);// 模板id
        // 模板
        if (!empty($msgId)) {
            $data['msgid'] = $msgId;
        }
        // 定时发送时间
        if (!empty($sendTime)) {
            $data['sendtime'] = $sendTime;
        }
        // 子签名
        if (!empty($subCode)) {
            $data['subcode'] = $subCode;
        }
        if (!empty($all['phones']) && is_array($all['phones'])) {
            $data['phones'] = implode(',', array_merge([$data['phones']], $all['phones']));
        }
        // 配置发送账号
        $account = $this->account($config);
        $data['account'] = $all['account'] ?? $account['account'];
        $data['password'] = $all['password'] ?? $account['password'];
        return $data;
    }

    /**
     * 获取消息id
     * @param array $data
     * @return string|null
     */
    protected function getMsgId(array $data)
    {
        if(!empty($data['msgid']) && is_string($data['msgid'])){
            return (string)$data['msgid'];
        }
        return null;
    }

    /**
     * 获取定时发送时间
     * @param array $params
     * @return false|mixed|string
     */
    protected function getSendTime(array $params)
    {
        $time = $params['sendtime'] ?? '';
        $format = substr(self::SEND_TIME_FORMAT, 0, 6);
        if (is_numeric($time) && false == strpos(date($format), $time)) {
            if ($time > strtotime('1970')) {
                return date(self::SEND_TIME_FORMAT, $time);
            }
            return '';
        }
        return $time;
    }

    /**
     * 获取签名
     * @param string|null $content
     * @return string
     */
    public function content(string $content = null): string
    {
        return preg_replace('/(【.+】)/', '', $content);
    }

    /**
     * 获取短信状态报告
     * @param Config $config
     * @return array
     */
    public function getSmsReport(Config $config = null)
    {
        if (empty($config)) {
            $config = $this->config;
        }
        $data = [
            'account' => $config->get('account'),
            'password' => $config->get('password'),
        ];
        return self::toArray($this->postJson($this->getApi("/Report"), $data));
    }

    /**
     * 获取手机回复的上行短信
     * @param Config $config
     * @return array
     */
    public function getSms(Config $config = null)
    {
        if (empty($config)) {
            $config = $this->config;
        }
        $data = [
            'account' => $config->get('account'),
            'password' => $config->get('password'),
        ];
        return self::toArray($this->postJson($this->getApi("/Deliver"), $data));
    }

    /**
     * 查询账户余额
     * @param Config $config
     * @return array
     */
    public function getBalance(Config $config = null)
    {
        if (empty($config)) {
            $config = $this->config;
        }
        $data = [
            'account' => $config->get('account'),
            'password' => $config->get('password'),
        ];
        return self::toArray($this->postJson($this->getApi("/Balance"), $data));
    }

    /**
     * 检测敏感词
     * @param string $content
     * @param Config $config
     * @return array
     */
    public function checkKeyword(string $content, Config $config = null)
    {
        if (empty($config)) {
            $config = $this->config;
        }
        $data = [
            'account' => $config->get('account'),
            'password' => $config->get('password'),
            'content' => $content,
        ];
        return self::toArray($this->postJson($this->getApi("/KeywordCheck"), $data));
    }

    /**
     * 检测黑名单
     * @param array $phones
     * @param Config $config
     * @return array
     */
    public function checkBlackList(array $phones, Config $config = null)
    {
        if (empty($config)) {
            $config = $this->config;
        }
        if (empty($phones)) {
            return [];
        }
        $data = [
            'account' => $config->get('account'),
            'password' => $config->get('password'),
        ];
        if (1 == count($phones)) {
            $data['content'] = trim(current($phones));
        } else {
            $data['content'] = trim(implode(',', $phones));
        }
        return self::toArray($this->postJson($this->getApi("/BlackListCheck"), $data));
    }
}