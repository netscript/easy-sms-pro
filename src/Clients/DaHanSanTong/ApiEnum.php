<?php
/**
 * Project Sms
 * file: ApiEnum.php
 * User: weblinuxgame
 * Date: 2019/6/18
 * Time: 14:38
 */

namespace WebLinuxGame\Sms\Clients\DaHanSanTong;

/**
 * 接口枚举
 * Interface ApiEnum
 * @package WebLinuxGame\Sms\Clients\DaHanSanTong
 */
interface ApiEnum
{
    const BASE_SMS_URL = 'http://www.dh3t.com/json/sms'; // 发送短信

    const API_SMS_QUERY_URL = 'http://dh3t.com/http/tjreport/querySms'; // 短信统计接口

    const API_MMS_URL = 'http://mms.3tong.net/http/mms'; // 彩信

    const API_VOICE_BASE_URL = 'http://voice.3tong.net/json/voiceSms'; // [主路由]语音接口

    const API_SHORT_GET_URL_ = 'http://www.dh3t.com/json/sms/GetShortUrl';// 生成短地址

    const API_SHORT_SEND_URL_ = 'http://www.dh3t.com/json/sms/ShortUrlSend';// 短地址发送

    const API_SHORT_INFO_URL = 'http://rest.3tong.net/http/rest/getNumInfo'; // 短地址详情推送

    const API_TEMPLATE_QUERY = 'http://www.dh3t.com/json/sms/show/template';// 模板查询接口

    const API_TEMPLATE_UPDATE = 'http://www.dh3t.com/json/sms/upload/template';// 模板更新创建

    const API_TEMPLATE_DELETE = 'http://www.dh3t.com/json/sms/delete/template';// 模板删除

    const API_PHONE_INFO = 'http://rest.3tong.net/http/rest/getNumInfo';

    const API_PHONE_VERY = 'http://rest.3tong.net/http/rest/numVerifi';

    const API_VIDEO_SMS_URL = 'http://mms.3tong.net/http/video/mms';
}