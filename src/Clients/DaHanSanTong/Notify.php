<?php
/**
 * Project Sms
 * file: Notify.php
 * User: weblinuxgame
 * Date: 2019/6/18
 * Time: 15:41
 */

namespace WebLinuxGame\Sms\Clients\DaHanSanTong;

/**
 * Class Notify
 * @package WebLinuxGame\Sms\Clients\DaHanSanTong
 */
class Notify
{
    private $request;

    /**
     * 消息
     * Notify constructor.
     * @param array $request
     */
    public function __construct(array $request)
    {
        $this->request = $request;
    }

    /**
     * 获取通知消息
     * @return array
     */
    public function getReports(): array
    {
        if (empty($this->request['reports'])) {
            return [];
        }
        return (array)$this->request['reports'];
    }

    /**
     * 是否成功
     * @return bool
     */
    public function isSuccess(): bool
    {
        if (empty($this->request['result']) && '成功' == $this->request['desc']) {
            return true;
        }
        return false;
    }
}